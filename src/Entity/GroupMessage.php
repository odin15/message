<?php

namespace App\Entity;

use App\Repository\GroupMessageRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GroupMessageRepository::class)]
class GroupMessage extends Message
{
    #[ORM\ManyToOne(targetEntity: Group::class, inversedBy: "messages")]
    private Group $group;

    public function getGroup(): Group
    {
        return $this->group;
    }

    public function setGroup(Group $group): self
    {
        $this->group = $group;
        return $this;
    }
}
