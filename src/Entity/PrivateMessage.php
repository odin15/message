<?php

namespace App\Entity;
use App\Repository\PrivateMessageRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PrivateMessageRepository::class)]
class PrivateMessage extends Message
{
    #[ORM\Column(type: 'integer')]
    private int $recipientId;

    public function getRecipientId(): int
    {
        return $this->recipientId;
    }

    public function setRecipientId(int $recipientId): self
    {
        $this->recipientId = $recipientId;
        return $this;
    }
}
