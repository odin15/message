<?php

namespace App\Entity;

use App\Repository\MessageRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MessageRepository::class)]
#[ORM\MappedSuperclass]
#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorColumn(name: 'discr', type: 'string')]
#[ORM\DiscriminatorMap([
    'group_message' => GroupMessage::class,
    'private_message' => PrivateMessage::class
])]
abstract class Message
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 255)]
    private string $content;

    #[ORM\Column(type: 'datetime')]
    private DateTime $creationDateTime;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?DateTime $lastUpdateDateTime;

    #[ORM\Column(type: 'integer')]
    private int $userId;

    public function getId(): int
    {
        return $this->id;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;
        return $this;
    }

    public function getCreationDateTime(): DateTime
    {
        return $this->creationDateTime;
    }

    public function setCreationDateTime(DateTime $creationDateTime): self
    {
        $this->creationDateTime = $creationDateTime;
        return $this;
    }

    public function getLastUpdateDateTime(): ?DateTime
    {
        return $this->lastUpdateDateTime;
    }

    public function setLastUpdateDateTime(?DateTime $lastUpdateDateTime): self
    {
        $this->lastUpdateDateTime = $lastUpdateDateTime;
        return $this;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;
        return $this;
    }
}
