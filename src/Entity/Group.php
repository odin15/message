<?php

namespace App\Entity;

use App\Repository\GroupRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GroupRepository::class)]
#[ORM\Table(name: '`group`')]
class Group
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    #[ORM\Column(type: 'integer')]
    private int $ownerId;

    #[ORM\OneToMany(mappedBy: 'group', targetEntity: UserGroup::class, cascade: ['persist', 'remove'])]
    private iterable $usersGroups = [];

    #[ORM\OneToMany(mappedBy: 'group', targetEntity: GroupMessage::class, cascade: ['persist', 'remove'])]
    private iterable $messages = [];

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getOwnerId(): int
    {
        return $this->ownerId;
    }

    public function setOwnerId(int $ownerId): self
    {
        $this->ownerId = $ownerId;
        return $this;
    }

    public function getUsersGroups(): iterable
    {
        return $this->usersGroups;
    }

    public function setUsersGroups(iterable $users): self
    {
        $this->usersGroups = [];

        foreach ($users as $user) {
            $this->addUser($user);
        }

        return $this;
    }

    public function addUser(UserGroup $userGroup): self
    {
        $userGroup->setGroup($this);
        $this->usersGroups[] = $userGroup;
        return $this;
    }

    public function getMessages(): iterable
    {
        return $this->messages;
    }

    public function setMessages(iterable $messages): self
    {
        $this->messages = $messages;
        return $this;
    }
}
