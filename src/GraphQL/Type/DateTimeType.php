<?php

namespace App\GraphQL\Type;

use GraphQL\Language\AST\Node;
use GraphQL\Type\Definition\ScalarType;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;

class DateTimeType extends ScalarType implements AliasedInterface
{
    public static function getAliases(): array
    {
        return ['DateTime'];
    }

    public function serialize($value)
    {
        return $value->format('Y-m-d H:i:s');
    }

    public function parseValue($value)
    {
        return new \DateTime($value);
    }

    public function parseLiteral($valueNode, ?array $variables = null)
    {
        return new \DateTime($valueNode->value);
    }
}
