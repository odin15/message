<?php

namespace App\GraphQL\Mutation;

use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Error\UserError;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Symfony\Contracts\Service\Attribute\Required;

class AbstractMutation implements MutationInterface
{
    protected EntityManagerInterface $entityManager;

    #[Required]
    public function setEntityManager(EntityManagerInterface $entityManager): self
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    protected function getEntity(
        string $class,
        int $id,
        string|null $errorMessage = null,
        int|null $errorCode = 0
    ) {
        $entity = $this->entityManager->getRepository($class)->find($id);

        if (null === $entity) {
            throw new UserError($errorMessage ?? "Entity '$class' not found", $errorCode);
        }

        return $entity;
    }
}
