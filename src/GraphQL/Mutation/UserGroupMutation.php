<?php

namespace App\GraphQL\Mutation;

use App\Business\UserGroupBusiness;
use App\Entity\Group;
use App\Entity\UserGroup;
use App\Wamp\Publisher\UserGroupPublisher;
use Exception;
use Symfony\Contracts\Service\Attribute\Required;

class UserGroupMutation extends AbstractMutation
{
    private UserGroupBusiness $userGroupBusiness;

    private UserGroupPublisher $userGroupPublisher;

    #[Required]
    public function setUserGroupBusiness(UserGroupBusiness $userGroupBusiness): self
    {
        $this->userGroupBusiness = $userGroupBusiness;
        return $this;
    }

    #[Required]
    public function setUserGroupPublisher(UserGroupPublisher $userGroupPublisher): self
    {
        $this->userGroupPublisher = $userGroupPublisher;
        return $this;
    }

    /**
     * @throws Exception
     */
    public function createUserGroup(array $input): int
    {
        $userGroup = $this->convertUserGroup($input);

        if (!$this->userGroupBusiness->isUserGroupCreatable($userGroup)) {
            throw new Exception('You can\'t create this user group');
        }

        $this->entityManager->persist($userGroup);
        $this->entityManager->flush();

        $this->userGroupPublisher->createUserGroup($userGroup);

        return $userGroup->getId();
    }

    /**
     * @throws Exception
     */
    public function updateUserGroup(array $input): bool
    {
        $userGroup = $this->getEntity(UserGroup::class, $input['id'], 'USER_GROUP_NOT_FOUND');

        if (!$this->userGroupBusiness->isUserGroupUpdatable($userGroup)) {
            throw new Exception('You can\'t update this user group');
        }

        $this->convertUserGroup($input, $userGroup);
        $this->entityManager->persist($userGroup);
        $this->entityManager->flush();

        $this->userGroupPublisher->updateUserGroup($userGroup);

        return true;
    }

    /**
     * @throws Exception
     */
    public function deleteUserGroup(int $id): bool
    {
        $userGroup = $this->getEntity(UserGroup::class, $id, 'USER_GROUP_NOT_FOUND');

        if (!$this->userGroupBusiness->isUserGroupDeletable($userGroup)) {
            throw new Exception('You can\'t delete this user group');
        }

        $groupId = $userGroup->getGroup()->getId();

        $this->entityManager->remove($userGroup);
        $this->entityManager->flush();

        $this->userGroupPublisher->deleteUserGroup($id, $groupId);

        return true;
    }

    public function convertUserGroup(array $input, UserGroup $userGroup = null): UserGroup
    {
        if (null === $userGroup) {
            $userGroup = (new UserGroup())
                ->setUserId($input['userId'])
                ->setGroup($this->getEntity(Group::class, $input['groupId'], 'GROUP_NOT_FOUND'))
            ;
        }

        $userGroup
            ->setName($input['name'])
        ;

        return $userGroup;
    }
}
