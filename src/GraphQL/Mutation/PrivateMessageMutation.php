<?php

namespace App\GraphQL\Mutation;

use App\Business\MessageBusiness;
use App\Entity\PrivateMessage;
use App\Wamp\Publisher\PrivateMessagePublisher;
use Odevia\MicroserviceServerBundle\Security\Security;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Contracts\Service\Attribute\Required;

class PrivateMessageMutation extends AbstractMutation
{
    private MessageBusiness $messageBusiness;

    private Security $security;

    private PrivateMessagePublisher $privateMessagePublisher;

    #[Required]
    public function setMessageBusiness(MessageBusiness $messageBusiness): self
    {
        $this->messageBusiness = $messageBusiness;
        return $this;
    }

    #[Required]
    public function setSecurity(Security $security): self
    {
        $this->security = $security;
        return $this;
    }

    #[Required]
    public function setPrivateMessagePublisher(PrivateMessagePublisher $privateMessagePublisher): self
    {
        $this->privateMessagePublisher = $privateMessagePublisher;
        return $this;
    }

    public function createPrivateMessage(array $input): int
    {
        $privateMessage = $this->convertPrivateMessage($input);

        if (!$this->messageBusiness->isMessageCreatable($privateMessage)) {
            throw new AccessDeniedHttpException('You can\'t create this message');
        }

        $this->entityManager->persist($privateMessage);
        $this->entityManager->flush();

        $this->privateMessagePublisher->createPrivateMessage($privateMessage);

        return $privateMessage->getId();
    }

    public function updatePrivateMessage(array $input): bool
    {
        $privateMessage = $this->getEntity(PrivateMessage::class, $input['id'], 'PRIVATE_MESSAGE_NOT_FOUND');
        $this->convertPrivateMessage($input, $privateMessage);

        if (!$this->messageBusiness->isMessageUpdatable($privateMessage)) {
            throw new AccessDeniedHttpException('You can\'t update this message');
        }

        $this->entityManager->persist($privateMessage);
        $this->entityManager->flush();

        $this->privateMessagePublisher->updatePrivateMessage($privateMessage);

        return true;
    }

    public function deletePrivateMessage(int $id): bool
    {
        $privateMessage = $this->getEntity(PrivateMessage::class, $id, 'PRIVATE_MESSAGE_NOT_FOUND');

        if (!$this->messageBusiness->isMessageDeletable($privateMessage)) {
            throw new AccessDeniedHttpException('You can\'t delete this message');
        }

        $id = $privateMessage->getId();
        $userId = $privateMessage->getUserId();
        $recipientId = $privateMessage->getRecipientId();


        $this->entityManager->remove($privateMessage);
        $this->entityManager->flush();

        $this->privateMessagePublisher->deletePrivateMessage($id, $userId, $recipientId);

        return true;
    }

    public function convertPrivateMessage(array $input, PrivateMessage $privateMessage = null): PrivateMessage
    {
        if (null === $privateMessage) {
            $privateMessage = (new PrivateMessage())
                ->setUserId($this->security->getUserId())
                ->setRecipientId($input['recipientId']);
        }

        $privateMessage
            ->setContent($input['content']);

        return $privateMessage;
    }
}
