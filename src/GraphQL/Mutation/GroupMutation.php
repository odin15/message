<?php

namespace App\GraphQL\Mutation;

use App\Business\GroupBusiness;
use App\Entity\Group;
use App\Entity\UserGroup;
use App\Wamp\Publisher\GroupPublisher;
use Odevia\MicroserviceServerBundle\Security\Security;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Contracts\Service\Attribute\Required;

class GroupMutation extends AbstractMutation
{
    private GroupBusiness $groupBusiness;

    private Security $security;

    private GroupPublisher $groupPublisher;

    #[Required]
    public function setGroupBusiness(GroupBusiness $groupBusiness): self
    {
        $this->groupBusiness = $groupBusiness;
        return $this;
    }

    #[Required]
    public function setSecurity(Security $security): self
    {
        $this->security = $security;
        return $this;
    }

    #[Required]
    public function setGroupPublisher(GroupPublisher $groupPublisher): self
    {
        $this->groupPublisher = $groupPublisher;
        return $this;
    }

    public function createGroup(array $input): int
    {
        $group = $this->convertGroup($input);

        if (!$this->groupBusiness->isGroupCreatable($group)) {
            throw new AccessDeniedHttpException('You can\'t create this group');
        }

        $this->entityManager->persist($group);
        $this->entityManager->flush();

        $this->groupPublisher->createGroup($group);

        return $group->getId();
    }

    public function updateGroup(array $input): bool
    {
        $group = $this->getEntity(Group::class, $input['id'], 'GROUP_NOT_FOUND');

        // Check if the group is updatable before updating it (if the owner changes, for example)
        if (!$this->groupBusiness->isGroupUpdatable($group)) {
            throw new AccessDeniedHttpException('You can\'t update this group');
        }

        $this->convertGroup($input, $group);

        $this->entityManager->persist($group);
        $this->entityManager->flush();

        $this->groupPublisher->updateGroup($group);

        return true;
    }

    public function deleteGroup(int $id): bool
    {
        $group = $this->getEntity(Group::class, $id, 'GROUP_NOT_FOUND');

        if (!$this->groupBusiness->isGroupDeletable($group)) {
            throw new AccessDeniedHttpException('You can\'t delete this group');
        }

        $this->entityManager->remove($group);
        $this->entityManager->flush();

        $this->groupPublisher->deleteGroup($id);

        return true;
    }

    public function convertGroup(array $input, Group $group = null): Group
    {
        if (null === $group) {
            $users = [];
            foreach($input['usersGroups'] as $user) {
                $users[] = (new UserGroup())
                    ->setName($user['name'])
                    ->setUserId($user['userId'])
                ;
            }

            $group = (new Group())
                ->setOwnerId($this->security->getUserId())
                ->setUsersGroups($users)
            ;
        }

        $group
            ->setName($input['name'])
        ;

        return $group;
    }
}
