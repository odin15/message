<?php

namespace App\GraphQL\Mutation;

use App\Business\MessageBusiness;
use App\Entity\Group;
use App\Entity\GroupMessage;
use App\Wamp\Publisher\GroupMessagePublisher;
use Odevia\MicroserviceServerBundle\Security\Security;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Contracts\Service\Attribute\Required;

class GroupMessageMutation extends AbstractMutation
{
    private MessageBusiness $messageBusiness;

    private Security $security;

    private GroupMessagePublisher $groupMessagePublisher;

    #[Required]
    public function setSecurity(Security $security): self
    {
        $this->security = $security;
        return $this;
    }

    #[Required]
    public function setMessageBusiness(MessageBusiness $messageBusiness): self
    {
        $this->messageBusiness = $messageBusiness;
        return $this;
    }

    #[Required]
    public function setGroupMessagePublisher(GroupMessagePublisher $groupMessagePublisher): self
    {
        $this->groupMessagePublisher = $groupMessagePublisher;
        return $this;
    }

    public function createGroupMessage(array $input): int
    {
        $groupMessage = $this->convertGroupMessage($input);

        if (!$this->messageBusiness->isMessageCreatable($groupMessage)) {
            throw new AccessDeniedHttpException('You can\'t create this message in this group');
        }

        $this->entityManager->persist($groupMessage);
        $this->entityManager->flush();

        $this->groupMessagePublisher->createGroupMessage($groupMessage);

        return $groupMessage->getId();
    }

    public function updateGroupMessage(array $input): bool
    {
        $groupMessage = $this->getEntity(GroupMessage::class, $input['id'], 'GROUP_MESSAGE_NOT_FOUND');

        if (!$this->messageBusiness->isMessageUpdatable($groupMessage)) {
            throw new AccessDeniedHttpException('You can\'t update this message in this group');
        }

        $this->convertGroupMessage($input, $groupMessage);

        $this->entityManager->persist($groupMessage);
        $this->entityManager->flush();

        $this->groupMessagePublisher->updateGroupMessage($groupMessage);

        return true;
    }

    public function deleteGroupMessage(int $id): bool
    {
        /** @var GroupMessage $groupMessage */
        $groupMessage = $this->getEntity(GroupMessage::class, $id, 'GROUP_MESSAGE_NOT_FOUND');

        if (!$this->messageBusiness->isMessageDeletable($groupMessage)) {
            throw new AccessDeniedHttpException('You can\'t delete this message in this group');
        }

        $groupId = $groupMessage->getGroup()->getId();
        $this->entityManager->remove($groupMessage);
        $this->entityManager->flush();

        $this->groupMessagePublisher->deleteGroupMessage($id, $groupId);

        return true;
    }

    private function convertGroupMessage(array $input, GroupMessage $groupMessage = null): GroupMessage
    {
        if (null === $groupMessage) {
            $group = $this->getEntity(Group::class, $input['groupId'], 'GROUP_NOT_FOUND');
            $groupMessage = (new GroupMessage())
                ->setGroup($group)
                ->setUserId($this->security->getUserId());
        }

        $groupMessage->setContent($input['content']);

        return $groupMessage;
    }
}
