<?php

namespace App\GraphQL\Query;

use App\Business\GroupBusiness;
use App\Entity\Group;
use App\Entity\UserGroup;
use Exception;
use Symfony\Contracts\Service\Attribute\Required;

class UserGroupQuery extends AbstractQuery
{
    private GroupBusiness $groupBusiness;

    #[Required]
    public function setGroupBusiness(GroupBusiness $groupBusiness): self
    {
        $this->groupBusiness = $groupBusiness;
        return $this;
    }

    public function getClass(): string
    {
        return UserGroup::class;
    }

    /**
     * @throws Exception
     */
    public function findUserGroupInGroup(int $groupId): array
    {
        $group = $this->getRepository(Group::class)->find($groupId);

        if (null === $group) {
            throw new Exception("Group with id $groupId not found");
        }

        if(!$this->groupBusiness->isGroupMember($group)) {
            throw new Exception("You are not a member of this group");
        }

        return $this->getRepository(UserGroup::class)->findBy(['group' => $group]);
    }
}
