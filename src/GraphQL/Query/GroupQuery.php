<?php

namespace App\GraphQL\Query;

use App\Entity\Group;
use Odevia\MicroserviceServerBundle\Security\Security;
use Symfony\Contracts\Service\Attribute\Required;

class GroupQuery extends AbstractQuery
{
    private Security $security;

    #[Required]
    public function setSecurity(Security $security): self
    {
        $this->security = $security;
        return $this;
    }

    public function getClass(): string
    {
        return Group::class;
    }

    public function findPersonalGroups(): array
    {
        return $this->getRepository($this->getClass())->findPersonalGroups($this->security->getUserId());
    }
}
