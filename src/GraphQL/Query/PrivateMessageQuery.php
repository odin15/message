<?php

namespace App\GraphQL\Query;

use App\Entity\PrivateMessage;
use Odevia\MicroserviceServerBundle\Security\Security;
use Overblog\GraphQLBundle\Definition\Argument;
use Symfony\Contracts\Service\Attribute\Required;

class PrivateMessageQuery extends AbstractQuery
{
    private Security $security;

    #[Required]
    public function setSecurity(Security $security): self
    {
        $this->security = $security;
        return $this;
    }

    protected function getClass(): string
    {
        return PrivateMessage::class;
    }

    public function findBy(Argument $arguments = null): array
    {
        if (empty($arguments)) {
            return [];
        }

        $arguments = $arguments->getArrayCopy();

        return $this->entityManager->getRepository($this->getClass())->findByUserId($this->security->getUserId(), $arguments['recipientId'], $arguments['currentId'], $arguments['maxResult']);
    }
}
