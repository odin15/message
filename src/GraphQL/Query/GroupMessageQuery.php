<?php

namespace App\GraphQL\Query;

use App\Business\GroupBusiness;
use App\Entity\Group;
use App\Entity\GroupMessage;
use Overblog\GraphQLBundle\Definition\Argument;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Contracts\Service\Attribute\Required;

class GroupMessageQuery extends AbstractQuery
{
    private GroupBusiness $groupBusiness;

    #[Required]
    public function setGroupBusiness(GroupBusiness $groupBusiness): self
    {
        $this->groupBusiness = $groupBusiness;
        return $this;
    }

    protected function getClass(): string
    {
        return GroupMessage::class;
    }

    public function search(Argument $argument): array
    {
        $arguments = $argument->getArrayCopy();
        $group = $this->entityManager->getRepository(Group::class)->find($arguments['groupId']);

        if (!$this->groupBusiness->isGroupMember($group)) {
            throw new AccessDeniedHttpException('You are not a member of this group');
        }

        return call_user_func_array([$this->getRepository($this->getClass()), 'search'], $arguments);
    }
}
