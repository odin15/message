<?php

namespace App\GraphQL\Query;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\QueryInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Contracts\Service\Attribute\Required;

abstract class AbstractQuery implements QueryInterface
{
    protected EntityManagerInterface $entityManager;

    protected abstract function getClass(): string;

    #[Required]
    public function setEntityManager(EntityManagerInterface $entityManager): self
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    public function getRepository(string|null $class = null): ObjectRepository
    {
        return $this->entityManager->getRepository(null === $class ? $this->getClass() : $class);
    }

    public function __invoke(ResolveInfo $info, $value, Argument $arguments)
    {
        if (method_exists($this, $info->fieldName)) {
            $method = $info->fieldName;

            return $this->$method($value, $arguments);
        }

        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        return $propertyAccessor->getValue($value, $info->fieldName);
    }

    public function find(int $id)
    {
        return $this->getRepository()->find($id);
    }

    public function findBy(Argument $arguments = null): array
    {
        return $this->getRepository()->findBy($arguments->getArrayCopy());
    }
}
