<?php

namespace App\EventListener\Listener;

use App\Entity\Message;
use DateTime;

class MessageListener
{
    public function prePersist(Message $message): void
    {
        $message->setCreationDateTime(new DateTime());
    }

    public function preUpdate(Message $message): void
    {
        $message->setLastUpdateDateTime(new DateTime());
    }
}
