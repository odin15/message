<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class GroupMessageRepository extends EntityRepository
{
    public function search(int $groupId, int $maxResults = null, int $lastId = null, bool $reverse = false): array
    {
        $queryBuilder = $this->createQueryBuilder('group_message')
            ->join('group_message.group', 'g');

        $queryBuilder
            ->where($queryBuilder->expr()->eq('g.id', ':groupId'));


        $queryBuilder
            ->setMaxResults($maxResults)
            ->setParameters([
                'groupId' => $groupId,
            ])
        ;

        if (null !== $lastId) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->{$reverse ? 'gt' : 'lt'}('group_message.id', ':lastId'))
                ->setParameter('lastId', $lastId);
        }
        return $queryBuilder->getQuery()->getResult();
    }
}
