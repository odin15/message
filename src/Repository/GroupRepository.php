<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class GroupRepository extends EntityRepository
{
    public function findPersonalGroups($userId): array
    {
        $queryBuilder = $this->createQueryBuilder('g');

        $queryBuilder->join('g.usersGroups', 'user_group')
            ->where($queryBuilder->expr()->eq('user_group.userId', ':userId'))
            ->setParameter('userId', $userId);

        return $queryBuilder->getQuery()->getResult();
    }
}
