<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class PrivateMessageRepository extends EntityRepository
{
    public function findByUserId(int $userId, int $recipientId, int $currentId = null, ?int $maxResult = null): array
    {
        if (null === $maxResult) {
            $maxResult = 100;
        }

        if ($userId === $recipientId) {
            return [];
        }

        $queryBuilder = $this->createQueryBuilder('private_message');
        $queryBuilder
            ->where(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->eq('private_message.recipientId', ':recipientId'),
                        $queryBuilder->expr()->eq('private_message.userId', ':userId')
                    ),
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->eq('private_message.recipientId', ':userId'),
                        $queryBuilder->expr()->eq('private_message.userId', ':recipientId')
                    )
                )
            )
            ->orderBy('private_message.creationDateTime', 'DESC')
            ->setParameters([
                'recipientId' => $recipientId,
                'userId' => $userId,
            ])
            ->setMaxResults($maxResult);

        if (null !== $currentId) {
            $queryBuilder
                ->andWhere(
                    $queryBuilder->expr()->lt('private_message.id', ':currentId')
                )
                ->setParameter('currentId', $currentId);
        }


        return $queryBuilder->getQuery()->getResult();
    }
}
