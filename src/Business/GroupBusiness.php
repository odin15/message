<?php

namespace App\Business;

use App\Entity\Group;
use Odevia\MicroserviceServerBundle\Security\Security;
use Symfony\Contracts\Service\Attribute\Required;

class GroupBusiness
{
    private Security $security;

    #[Required]
    public function setSecurity(Security $security): self
    {
        $this->security = $security;
        return $this;
    }

    public function isGroupMember(Group $group, int $userId = null): bool
    {
        if (null === $userId) {
            $userId = $this->security->getUserId();
        }

        foreach ($group->getUsersGroups() as $user) {
            if ($user->getUserId() === $userId) {
                return true;
            }
        }

        return false;
    }

    public function isUserGroupOwner(Group $group, int $userId = null): bool
    {
        return $group->getOwnerId() === ($userId ?? $this->security->getUserId());
    }

    public function isGroupCreatable(Group $group): bool
    {
        return $this->isUserGroupOwner($group);
    }

    public function isGroupUpdatable(Group $group): bool
    {
        return $this->isUserGroupOwner($group);
    }

    public function isGroupDeletable(Group $group): bool
    {
        return $this->isUserGroupOwner($group);
    }
}
