<?php

namespace App\Business;

use App\Entity\PrivateMessage;
use Odevia\MicroserviceServerBundle\Security\Security;
use Symfony\Contracts\Service\Attribute\Required;

class PrivateMessageBusiness
{
    private Security $security;

    #[Required]
    public function setSecurity(Security $security): self
    {
        $this->security = $security;
        return $this;
    }

    public function isMessageCreatable(PrivateMessage $message): bool
    {
        return $this->isCurrentUserMessageOwner($message);
    }

    public function isMessageDeletable(PrivateMessage $message): bool
    {
        return $this->isCurrentUserMessageOwner($message);
    }

    public function isMessageUpdatable(PrivateMessage $message): bool
    {
        return $this->isCurrentUserMessageOwner($message);
    }

    public function isCurrentUserMessageOwner(PrivateMessage $message): bool
    {
        return $this->security->getUserId() === $message->getUserId();
    }
}
