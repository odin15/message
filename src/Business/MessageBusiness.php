<?php

namespace App\Business;

use App\Entity\GroupMessage;
use App\Entity\Message;
use App\Entity\PrivateMessage;

class MessageBusiness
{
    private array $messageBusinesses;

    public function __construct(PrivateMessageBusiness $privateMessageBusiness, GroupMessageBusiness $groupMessageBusiness)
    {
        $this->messageBusinesses = [
            PrivateMessage::class => $privateMessageBusiness,
            GroupMessage::class => $groupMessageBusiness,
        ];
    }

    public function isMessageCreatable(Message $message): bool
    {
        return $this->messageBusinesses[$message::class]->isMessageCreatable($message);
    }

    public function isMessageUpdatable(Message $message): bool
    {
        return $this->messageBusinesses[$message::class]->isMessageUpdatable($message);
    }

    public function isMessageDeletable(Message $message): bool
    {
        return $this->messageBusinesses[$message::class]->isMessageDeletable($message);
    }
}
