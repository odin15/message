<?php

namespace App\Business;

use App\Entity\GroupMessage;
use Odevia\MicroserviceServerBundle\Security\Security;
use Symfony\Contracts\Service\Attribute\Required;

class GroupMessageBusiness
{
    private GroupBusiness $groupBusiness;

    private Security $security;

    #[Required]
    public function setGroupBusiness(GroupBusiness $groupBusiness): self
    {
        $this->groupBusiness = $groupBusiness;
        return $this;
    }

    #[Required]
    public function setSecurity(Security $security): self
    {
        $this->security = $security;
        return $this;
    }

    public function isMessageCreatable(GroupMessage $groupMessage): bool
    {
        return $this->groupBusiness->isGroupMember($groupMessage->getGroup());
    }

    public function isMessageUpdatable(GroupMessage $groupMessage): bool
    {
        return $groupMessage->getUserId() === $this->security->getUserId() &&
            $this->groupBusiness->isGroupMember($groupMessage->getGroup());
    }

    public function isMessageDeletable(GroupMessage $groupMessage): bool
    {
        return $groupMessage->getUserId() === $this->security->getUserId() &&
            $this->groupBusiness->isGroupMember($groupMessage->getGroup());
    }
}