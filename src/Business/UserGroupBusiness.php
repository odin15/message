<?php

namespace App\Business;

use App\Entity\UserGroup;
use Odevia\MicroserviceServerBundle\Security\Security;
use Symfony\Contracts\Service\Attribute\Required;

class UserGroupBusiness
{
    private GroupBusiness $groupBusiness;

    private Security $security;

    #[Required]
    public function setSecurity(Security $security): self
    {
        $this->security = $security;
        return $this;
    }

    #[Required]
    public function setGroupBusiness(GroupBusiness $groupBusiness): self
    {
        $this->groupBusiness = $groupBusiness;
        return $this;
    }

    public function isUserGroupCreatable(UserGroup $userGroup): bool
    {
        return $this->groupBusiness->isGroupMember($userGroup->getGroup()) &&
            !$this->groupBusiness->isGroupMember($userGroup->getGroup(), $userGroup->getUserId());
    }

    public function isUserGroupUpdatable(UserGroup $userGroup): bool
    {
        return $this->groupBusiness->isUserGroupOwner($userGroup->getGroup()) ||
            $userGroup->getUserId() === $this->security->getUserId();
    }

    public function isUserGroupDeletable(UserGroup $userGroup): bool
    {
        return (
                $this->groupBusiness->isUserGroupOwner($userGroup->getGroup()) ||
                $userGroup->getUserId() === $this->security->getUserId()
            ) && !$this->groupBusiness->isUserGroupOwner($userGroup->getGroup(), $userGroup->getUserId());
    }
}
