<?php

namespace App\Wamp\Subscriber;

interface SubscriberInterface
{
    public function onMessage(array $parameters, \stdClass $payload);
}
