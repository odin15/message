<?php

namespace App\Wamp\Client;

use App\Wamp\Client\Authenticator\ClientAuthenticator;
use Psr\Log\LoggerInterface;
use React\Promise\Deferred;
use React\Promise\PromiseInterface;
use Symfony\Contracts\Service\Attribute\Required;
use Thruway\CallResult;
use Thruway\ClientSession;
use Thruway\Logging\Logger;
use Thruway\Peer\Client;
use Thruway\Transport\PawlTransportProvider;


class ClientManager
{

    public function __construct(
        private ClientAuthenticator $clientAuthenticator,
        private string              $wampServerUrl
    )
    {
    }

    #[Required]
    public function setLogger(LoggerInterface $logger): void
    {
        Logger::set($logger);
    }

    public function publish(string $realm, string $topic, array $argsKw = [], array $options = []): PromiseInterface
    {
        $wampClient = $this->createClient($realm);

        if (!isset($options['acknowledge'])) {
            $options['acknowledge'] = true;
        }

        $deferred = new Deferred();
        $wampClient->on('open', function (ClientSession $session) use ($deferred, $topic, $argsKw, $options) {
            /** @var PromiseInterface|false $promise */
            $promise = $session->publish($topic, [], $argsKw, $options);

            if (false !== $promise) {
                $promise->then(function () use ($deferred, $session) {
                    $session->close();
                    $deferred->resolve();
                });
            } else {
                $session->close();
                $deferred->resolve();
            }
        });

        $wampClient->start();

        return $deferred->promise();
    }

    public function call(string $realm, string $procedureName, array $argsKw = [], array $options = []): PromiseInterface
    {
        $wampClient = $this->createClient($realm);

        $deferred = new Deferred();
        $pending = true;
        $wampClient->on('open', function (ClientSession $session) use ($deferred, $procedureName, $argsKw, $options) {
            $session
                ->call($procedureName, [], $argsKw, $options)
                ->then(function (CallResult $result) use ($deferred, $session, &$pending) {
                    $session->close();
                    $deferred->resolve(json_decode(json_encode($result->getArgumentsKw()), true));
                    $pending = false;
                }, function ($data) use ($deferred, $session, &$pending) {
                    $session->close();
                    $deferred->reject($data);
                    $pending = false;
                });
        });

        $wampClient->start();
        $promise = $deferred->promise();

        if ($pending) {
            $deferred->reject('timeout');
        }

        return $promise;
    }

    private function createClient(string $realm): Client
    {
        $wampClient = new WampClient($realm);
        $wampClient->setAttemptRetry(false);

        $transportProvider = new PawlTransportProvider($this->wampServerUrl);
        $wampClient->addTransportProvider($transportProvider);
        $wampClient->addClientAuthenticator($this->clientAuthenticator);

        return $wampClient;
    }

}
