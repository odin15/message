<?php

namespace App\Wamp\Client;

use App\Wamp\Register\RegisterInterface;
use App\Wamp\Subscriber\SubscriberInterface;
use App\Wamp\WampTopic;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use React\EventLoop\LoopInterface;
use React\Promise\Deferred;
use React\Promise\PromiseInterface;
use Symfony\Contracts\Service\Attribute\Required;
use Thruway\Peer\Client;
use Thruway\Result;


class WampClient extends Client
{
    use LoggerAwareTrait;

    /** @var SubscriberInterface[] */
    protected array $subscribers = [];
    /** @var RegisterInterface[] */
    protected array $registers = [];
    protected Deferred $onSessionStart;

    public function __construct($realm, LoopInterface $loop = null)
    {
        parent::__construct($realm, $loop);
        $this->onSessionStart = new Deferred();
    }

    #[Required]
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    public function addSubscriber(SubscriberInterface $subscriber, string $topicName)
    {
        $this->subscribers[$topicName] = $subscriber;
    }

    public function addRegister(RegisterInterface $register, string $topicName)
    {
        $this->registers[$topicName] = $register;
    }

    public function onSessionStart($session, $transport)
    {
        foreach ($this->subscribers as $topicName => $subscriber) {
            $this->subscribe(WampTopic::getTopic($topicName, $this->getWampTopicParameters()), [$subscriber, 'onMessage']);
        }

        foreach ($this->registers as $topicName => $register) {
            $this->register(WampTopic::getTopic($topicName, $this->getWampTopicParameters()), [$register, 'onCall']);
        }

        $this->onSessionStart->resolve(['session' => $session, 'transport' => $transport]);
    }

    public function register(string $topicName, $callable): void
    {
        $this->session->register($topicName, function (array $parameters, \stdClass $args) use ($callable) {
            try {
                $result = call_user_func_array($callable, [json_decode(json_encode($args), true)]);
                if ($result instanceof PromiseInterface) {
                    return $result->then(fn($data) => new Result(null, $data));
                }

                return new Result(null, $result);
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());

                return [
                    'result' => false,
                    'errors' => [$e->getMessage()],
                ];
            }
        });
    }

    public function subscribe(string $topicName, $callable): void
    {
        $this->session->subscribe($topicName, function ($args, $kwargs) use ($callable) {
            try {
                $callable($args, $kwargs);
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
            }
        });
    }

    public function publish($topicName, $arguments = [], $argumentsKw = []): PromiseInterface
    {
        return $this->getSession()->publish(
            $topicName,
            $arguments,
            $argumentsKw,
            ['acknowledge' => true]
        )->then(function ($value) {
            return $value;
        }, function ($error) {
            return $error;
        });
    }

    public function call(string $procedureName, $arguments = [], $argumentsKw = [], array $options = []): PromiseInterface
    {
        return $this->session->call($procedureName, $arguments, $argumentsKw, $options);
    }

    protected function getWampTopicParameters(): array
    {
        return [];
    }

    public function getOnSessionStart(): PromiseInterface
    {
        return $this->onSessionStart->promise();
    }

}