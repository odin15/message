<?php

namespace App\Wamp\Client\Authenticator;

use Thruway\Authentication\ClientWampCraAuthenticator;

class ClientAuthenticator extends ClientWampCraAuthenticator
{
    public function __construct(string $accessToken)
    {
        parent::__construct('message', $accessToken);
    }

    public function getAuthMethods(): array
    {
        return ['message_wampcra'];
    }
}
