<?php

namespace App\Wamp\Publisher;

use App\Wamp\Client\ClientManager;
use App\Wamp\WampTopic;
use React\Promise\PromiseInterface;
use Symfony\Contracts\Service\Attribute\Required;

abstract class AbstractPublisher
{
    private ClientManager $clientManager;

    #[Required]
    public function setClientManager(ClientManager $clientManager): self
    {
        $this->clientManager = $clientManager;
        return $this;
    }

    protected function publish($topicName, $arguments = [], $topicParameters = []): PromiseInterface
    {
        return $this->clientManager->publish('message', WampTopic::getTopic($topicName, $topicParameters), $arguments);
    }
}
