<?php

namespace App\Wamp\Publisher;

use App\Entity\Group;
use App\Entity\UserGroup;
use App\Wamp\WampTopic;
use React\Promise\PromiseInterface;
use function React\Promise\all;

class GroupPublisher extends AbstractPublisher
{
    public function createGroup(Group $group): PromiseInterface
    {
        $promises = [];
        $usersGroups = array_map(function (UserGroup $userGroup) {
            return [
                'id' => $userGroup->getId(),
                'userId' => $userGroup->getUserId(),
                'name' => $userGroup->getName(),
            ];
        }, iterator_to_array($group->getUsersGroups()));
        foreach ($group->getUsersGroups() as $usersGroup) {
            $promises[] = $this->publish(WampTopic::TOPIC_GROUP_CREATED, [
                'id' => $group->getId(),
                'name' => $group->getName(),
                'ownerId' => $group->getOwnerId(),
                'usersGroups' => $usersGroups,
            ], [
                'userId' => $usersGroup->getUserId(),
            ]);
        }

        return all($promises);
    }

    public function updateGroup(Group $group): PromiseInterface
    {
        return $this->publish(WampTopic::TOPIC_GROUP_UPDATED, [
            'id' => $group->getId(),
            'name' => $group->getName(),
            'ownerId' => $group->getOwnerId(),
        ], [
            'groupId' => $group->getId(),
        ]);
    }

    public function deleteGroup(int $groupId): PromiseInterface
    {
        return $this->publish(WampTopic::TOPIC_GROUP_DELETED, [
            'id' => $groupId,
        ], [
            'groupId' => $groupId,
        ]);
    }
}
