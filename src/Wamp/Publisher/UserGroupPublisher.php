<?php

namespace App\Wamp\Publisher;

use App\Entity\GroupMessage;
use App\Entity\UserGroup;
use App\Wamp\WampTopic;
use React\Promise\PromiseInterface;
use function React\Promise\all;

class UserGroupPublisher extends AbstractPublisher
{
    public function createUserGroup(UserGroup $userGroup): PromiseInterface
    {
        $group = $userGroup->getGroup();
        return all([
                $this->publish(WampTopic::TOPIC_USER_GROUP_CREATED,
                    [
                        'id' => $userGroup->getId(),
                        'userId' => $userGroup->getUserId(),
                        'name' => $userGroup->getName(),
                        'group' => [
                            'id' => $group->getId(),
                            'name' => $group->getName(),
                            'ownerId' => $group->getOwnerId(),
                            'messages' => array_map(function (GroupMessage $groupMessage) {
                                return [
                                    'id' => $groupMessage->getId(),
                                    'content' => $groupMessage->getContent(),
                                    'userId' => $groupMessage->getUserId(),
                                    'createdAt' => $groupMessage->getCreatedAt()->format('Y-m-d H:i:s'),
                                    'updatedAt' => $groupMessage->getUpdatedAt()->format('Y-m-d H:i:s'),
                                ];
                            }, iterator_to_array($group->getMessages())),
                        ],
                    ],
                    [
                        'userId' => $userGroup->getUserId(),
                    ]
                ),
                $this->publish(WampTopic::TOPIC_USER_GROUP_CREATED_GROUP,
                    [
                        'id' => $userGroup->getId(),
                        'userId' => $userGroup->getUserId(),
                        'name' => $userGroup->getName(),
                    ],
                    [
                        'groupId' => $group->getId(),
                    ]
                ),
            ]
        );
    }

    public function updateUserGroup(UserGroup $userGroup): PromiseInterface
    {
        return $this->publish(WampTopic::TOPIC_USER_GROUP_UPDATED, [
            'id' => $userGroup->getId(),
            'userId' => $userGroup->getUserId(),
            'name' => $userGroup->getName(),
        ], [
            'groupId' => $userGroup->getGroup()->getId(),
        ]);
    }

    public function deleteUserGroup(int $id, int $groupId): PromiseInterface
    {
        return $this->publish(WampTopic::TOPIC_USER_GROUP_DELETED, [
            'id' => $id,
        ], [
            'groupId' => $groupId,
        ]);
    }
}
