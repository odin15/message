<?php

namespace App\Wamp\Publisher;

use App\Entity\PrivateMessage;
use App\Wamp\WampTopic;
use React\Promise\PromiseInterface;
use function React\Promise\all;

class PrivateMessagePublisher extends AbstractPublisher
{
    public function createPrivateMessage(PrivateMessage $privateMessage): PromiseInterface
    {
        return all(array_map(function ($id) use ($privateMessage) {
            return $this->publish(WampTopic::TOPIC_PRIVATE_MESSAGE_CREATED, [
                'id' => $privateMessage->getId(),
                'userId' => $privateMessage->getUserId(),
                'recipientId' => $privateMessage->getRecipientId(),
                'content' => $privateMessage->getContent(),
                'creationDateTime' => $privateMessage->getCreationDateTime()->format('Y-m-d H:i:s'),
            ], [
                'userId' => $id,
            ]);
        }, [$privateMessage->getUserId(), $privateMessage->getRecipientId()]));
    }

    public function updatePrivateMessage(PrivateMessage $privateMessage): PromiseInterface
    {
        return all(array_map(function ($id) use ($privateMessage) {
            return $this->publish(WampTopic::TOPIC_PRIVATE_MESSAGE_DELETED, [
                'id' => $privateMessage->getId(),
                'content' => $privateMessage->getContent(),
                'lastUpdateDateTime' => $privateMessage->getLastUpdateDateTime()->format('Y-m-d H:i:s'),
            ], [
                'userId' => $id,
            ]);
        }, [$privateMessage->getUserId(), $privateMessage->getRecipientId()]));
    }

    public function deletePrivateMessage(int $id, int $userId, int $recipientId): PromiseInterface
    {
        return all(array_map(function ($userId) use ($id) {
            return $this->publish(WampTopic::TOPIC_PRIVATE_MESSAGE_DELETED, [
                'id' => $id,
            ], [
                'userId' => $userId,
            ]);
        }, [$userId, $recipientId]));
    }
}
