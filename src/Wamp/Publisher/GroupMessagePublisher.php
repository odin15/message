<?php

namespace App\Wamp\Publisher;

use App\Entity\GroupMessage;
use App\Wamp\WampTopic;
use React\Promise\PromiseInterface;

class GroupMessagePublisher extends AbstractPublisher
{
    public function createGroupMessage(GroupMessage $groupMessage): PromiseInterface
    {
        return $this->publish(WampTopic::TOPIC_GROUP_MESSAGE_CREATED, [
            'userId' => $groupMessage->getUserId(),
            'content' => $groupMessage->getContent(),
            'creationDateTime' => $groupMessage->getCreationDateTime()->format('Y-m-d H:i:s'),
        ], [
            'groupId' => $groupMessage->getGroup()->getId(),
        ]);
    }

    public function updateGroupMessage(GroupMessage $groupMessage): PromiseInterface
    {
        return $this->publish(WampTopic::TOPIC_GROUP_MESSAGE_UPDATED, [
            'id' => $groupMessage->getId(),
            'content' => $groupMessage->getContent(),
            'lastUpdateDateTime' => $groupMessage->getLastUpdateDateTime()->format('Y-m-d H:i:s'),
        ], [
            'groupId' => $groupMessage->getGroup()->getId(),
        ]);
    }

    public function deleteGroupMessage(int $id, int $groupId): PromiseInterface
    {
        return $this->publish(WampTopic::TOPIC_GROUP_MESSAGE_DELETED, [
            'id' => $id,
        ], [
            'groupId' => $groupId,
        ]);
    }
}
