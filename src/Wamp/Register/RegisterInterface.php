<?php

namespace App\Wamp\Register;

interface RegisterInterface
{
    public function onCall(array $arguments);
}
