<?php

namespace App\Wamp;

class WampTopic
{
    public const TOPIC_PRIVATE_MESSAGE_CREATED = 'private.message.created.{userId}';
    public const TOPIC_PRIVATE_MESSAGE_UPDATED = 'private.message.updated.{userId}';
    public const TOPIC_PRIVATE_MESSAGE_DELETED = 'private.message.deleted.{userId}';

    public const TOPIC_USER_GROUP_CREATED_GROUP = 'user_group.created.group.{groupId}';

    public const TOPIC_USER_GROUP_CREATED = 'user_group.created.{userId}';
    public const TOPIC_USER_GROUP_UPDATED = 'user_group.updated.{groupId}';
    public const TOPIC_USER_GROUP_DELETED = 'user_group.deleted.{groupId}';


    public const TOPIC_GROUP_CREATED = 'group.created.{userId}';
    public const TOPIC_GROUP_UPDATED = 'group.updated.{groupId}';
    public const TOPIC_GROUP_DELETED = 'group.deleted.{groupId}';

    public const TOPIC_GROUP_MESSAGE_CREATED = 'group.message.created.{groupId}';
    public const TOPIC_GROUP_MESSAGE_UPDATED = 'group.message.updated.{groupId}';
    public const TOPIC_GROUP_MESSAGE_DELETED = 'group.message.deleted.{groupId}';

    public static function getTopic(string $topic, array $parameters = []): string
    {
        foreach ($parameters as $name => $value) {
            $topic = str_replace('{'.$name.'}', $value, $topic);
        }

        return $topic;
    }
}
