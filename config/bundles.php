<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Overblog\GraphQLBundle\OverblogGraphQLBundle::class => ['all' => true],
    Odeven\AmqpBundle\OdevenAmqpBundle::class => ['all' => true],
    Odevia\MicroserviceClientBundle\OdeviaMicroserviceClientBundle::class => ['all' => true],
    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => ['all' => true],
    Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class => ['all' => true],
    Odevia\MicroserviceServerBundle\OdeviaMicroserviceServerBundle::class => ['all' => true],
];
